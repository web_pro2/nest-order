import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customer/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const customer = await this.customersRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.orderRepository.save(order);
    const orderItems: OrderItem[] = await Promise.all(
      createOrderDto.orderItems.map(async (od) => {
        const orderItems = new OrderItem();
        orderItems.amount = od.amount;
        orderItems.product = await this.productsRepository.findOneBy({
          id: od.productId,
        });
        orderItems.name = orderItems.product.name;
        orderItems.price = orderItems.product.price;
        orderItems.total = orderItems.price * orderItems.amount;
        orderItems.order = order;
        return orderItems;
      }),
    );

    for (const orderItem of orderItems) {
      this.orderItemsRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    // order.orderItems = orderItems;
    await this.orderRepository.save(order);
    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.orderRepository.find({ relations: ['customer', 'orderItems'] });
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    return this.orderRepository.softRemove(order);
  }
}
